//
//  MainStore.m
//  BuddyTag
//
//  Created by sj singh on 3/13/14.
//  Copyright (c) 2014 AppDar. All rights reserved.
//

#import "MainStore.h"

#define PUBNUB_LOCATION_UPDATE_CHANNEL @"location_update_channel"
#define PUBNUB_TAG_USER_CHANNEL @"tagged_channel"

@interface MainStore () <PNDelegate>

@end

@implementation MainStore

//Connect to Pubnub and subscribe to the two channels
- (void)connectToPubnub {
    [PubNub setDelegate:self];
    [PubNub setConfiguration:[PNConfiguration configurationForOrigin:@"pubsub.pubnub.com" publishKey:@"pub-c-273d4a8f-25ce-4aea-b0d9-a2145418dbb2"
                                                        subscribeKey:@"sub-c-862068e4-aaef-11e3-858b-02ee2ddab7fe" secretKey:@"sec-c-MzRjNTdhMGQtZjhmYy00NGE5LWEwZTQtYTUyYWQxMGEwZDAx"]];
    [PubNub connect];
    
    PNChannel *locationUpdateChannel = [PNChannel channelWithName:PUBNUB_LOCATION_UPDATE_CHANNEL shouldObservePresence:YES];
    PNChannel *tagUserChannel = [PNChannel channelWithName:PUBNUB_TAG_USER_CHANNEL shouldObservePresence:YES];
    [PubNub subscribeOnChannels:@[locationUpdateChannel, tagUserChannel]];

}

//The method to send the device location to other users
- (void)sendNewLocation:(CLLocation *)location {
    //Safety net
    if (![AppSession sharedSession].user) return;
    
    PNChannel *locationUpdateChannel = [PNChannel channelWithName:PUBNUB_LOCATION_UPDATE_CHANNEL shouldObservePresence:YES];
    
    NSDictionary *userPayload = @{@"id": [AppSession sharedSession].user.userID,
                                  @"name": [AppSession sharedSession].user.userDisplayName
                                  };
    //Logged in user on this device
    NSDictionary *locationPayload = @{@"lat": @(location.coordinate.latitude), @"lng": @(location.coordinate.longitude)};
    
    NSDictionary *payload = @{@"user": userPayload, @"location": locationPayload};
    
    [PubNub sendMessage:payload toChannel:locationUpdateChannel];
}

//The method to send a tag to another user
-(void)tagUser:(User *)user message:(NSString *)message{
    
    PNChannel *locationUpdateChannel = [PNChannel channelWithName:PUBNUB_TAG_USER_CHANNEL shouldObservePresence:YES];
    
    NSDictionary *userPayload = @{@"id": user.userID,
                                  @"name": user.userDisplayName
                                  };
    //Logged in user on this device
    NSDictionary *userTaggedByPayload = @{@"id": [AppSession sharedSession].user.userID,
                                  @"name": [AppSession sharedSession].user.userDisplayName
                                  };
    
    
    NSDictionary *payload = @{@"user": userPayload, @"taggedBy": userTaggedByPayload, @"message": message};
    
    [PubNub sendMessage:payload toChannel:locationUpdateChannel];
}

#pragma mark - Pubnub Delegates
//We have data from somewhere, from something, from some channel.
- (void)pubnubClient:(PubNub *)client didReceiveMessage:(PNMessage *)message {
    //if the message is not a json payload, ignore it.
    if (![message.message isKindOfClass:[NSDictionary class]]) return;
    
    if ([message.channel.name isEqualToString:PUBNUB_LOCATION_UPDATE_CHANNEL]){
        [self locationUpdateJSONRecieved: message.message];
    }else if ([message.channel.name isEqualToString:PUBNUB_TAG_USER_CHANNEL]){
        [self tagUserJSONRecieved: message.message];
    }
    
}

#pragma mark - Private methods

//Recieved a location update from a user.
//Map the json data to objects and send to the connected controller
- (void)locationUpdateJSONRecieved: (NSDictionary *)json {
    User *user = [User userFromJSONDict:json[@"user"]];
    Location *location = [Location locationFromDict:json[@"location"]];
    
    location.locationUser = user;
    
    if (self.delegate) [self.delegate updateUserLocation:location];
}

//We have a user that was tagged
//If the tagged user is not the logged in user then we do not need to do anything
//If it is the user then map the json and send it over to the controller
- (void)tagUserJSONRecieved: (NSDictionary *)json {
    User *user = [User userFromJSONDict:json[@"user"]];
    User *userTaggedBy = [User userFromJSONDict:json[@"taggedBy"]];
    NSString *message = json[@"message"];
    
    //The tagged user is not the current user, don't need to do anything
    if (![user isEqual:[AppSession sharedSession].user]) return;
    
    if (self.delegate)[self.delegate userTagged:userTaggedBy message:message];
    
}

@end
