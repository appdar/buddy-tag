//
//  User.h
//  BuddyTag
//
//  Created by sj singh on 3/13/14.
//  Copyright (c) 2014 AppDar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject <NSCoding>

@property (nonatomic, strong) NSNumber *userID;
@property (nonatomic, strong) NSString *userDisplayName;

//Tag the json dictionary, map it, & return user object
+ (User *) userFromJSONDict: (NSDictionary *) jsonDict;

@end
