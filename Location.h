//
//  Location.h
//  BuddyTag
//
//  Created by sj singh on 3/13/14.
//  Copyright (c) 2014 AppDar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import <CoreLocation/CoreLocation.h>

@interface Location : NSObject

@property (nonatomic, strong) User *locationUser;
@property (nonatomic) CLLocationCoordinate2D locationCoords;
@property (nonatomic, strong) NSDate *locationTimestamp;

//Tag the json dictionary, map it, & return user object
+ (Location *) locationFromDict: (NSDictionary *) jsonDict;

@end
