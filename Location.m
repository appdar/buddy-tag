//
//  Location.m
//  BuddyTag
//
//  Created by sj singh on 3/13/14.
//  Copyright (c) 2014 AppDar. All rights reserved.
//

#import "Location.h"

@implementation Location

+ (Location *) locationFromDict: (NSDictionary *) jsonDict {
    Location *location = [Location new];
    
    location.locationCoords = CLLocationCoordinate2DMake([jsonDict[@"lat"] doubleValue], [jsonDict[@"lng"] doubleValue]);
    
    return location;
}

-(BOOL)isEqual:(Location *)object {
    if (![object isKindOfClass:[Location class]]) return NO;
    
    if ([object.locationUser isEqual:self.locationUser]) return YES;
    else return NO;
}

@end
