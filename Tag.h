//
//  Tag.h
//  BuddyTag
//
//  Created by sj singh on 3/13/14.
//  Copyright (c) 2014 AppDar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface Tag : NSObject

@property (nonatomic, strong) User *tagFromUser;
@property (nonatomic, strong) User *tagToUser; //Would not need if messages were direct and not broadcast
@property (nonatomic, strong) NSString *tagMessage;


@end
