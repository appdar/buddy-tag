//
//  ViewController.m
//  BuddyTag
//
//  Created by sj singh on 3/13/14.
//  Copyright (c) 2014 AppDar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <CLLocationManagerDelegate, MKMapViewDelegate, MainStoreDelegate> {
    CLLocationManager *locationManager;
    
    NSMutableArray *annotations;
    Location *selectedLocation;
    MainStore *mainStore;
}

@property (nonatomic, strong) IBOutlet MKMapView *mapView;

@end

@implementation ViewController


#pragma mark - View Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Lets do some initizations and connect to pubnub's sockets
    annotations = [NSMutableArray new];
    mainStore = [MainStore new];
	mainStore.delegate = self;
    [mainStore connectToPubnub];
    
    
    //Starting location manager to track location so we can start sending it as soon as user logs in
    locationManager = [[CLLocationManager alloc] init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    locationManager.delegate = self;

    locationManager.distanceFilter = 4; // meters
    
    [locationManager startUpdatingLocation];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // If the user has not logged in yet, we need to display the loginViewController
    if (![AppSession sharedSession].user){
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"bundle:nil];
        LoginViewController *loginViewController = [storyboard instantiateViewControllerWithIdentifier:@"loginViewController"];
        [self presentViewController:loginViewController animated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

//Tag the selected user
//Optional custom message
- (IBAction)tagUser:(id)sender {
    [mainStore tagUser:selectedLocation.locationUser message:self.tagMessage.text];
    [self hideTagView];
    [self.tagMessage resignFirstResponder];
}

- (IBAction)closeTagView:(id)sender {
    [self hideTagView];
}

#pragma mark - Private Methods

- (void) showTagView {
    //Fail safe
    if (!selectedLocation) return;
    //Tagview is already open
    if (self.tagViewTrailingContraint.constant == 0) return; //already open
    
    self.tagViewTrailingContraint.constant = 0;
    //Animate contraint change
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void) hideTagView {

    self.tagViewTrailingContraint.constant = -self.tagView.frame.size.width;
    
    //Animate contraint change
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void) setTagViewUser{
    self.name.text = selectedLocation.locationUser.userDisplayName;
    
    //Fetch the address doing a reverse geocoding on the user coordinates
    CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
    CLLocation *location = [[CLLocation alloc] initWithLatitude:selectedLocation.locationCoords.latitude longitude:selectedLocation.locationCoords.longitude];
    [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        if (placemarks.count){
            
            CLPlacemark * placemark = placemarks[0];
            NSString *address = [NSString new];
            
            //Street number (ex: 123, 442-32)
            if (placemark.subThoroughfare){
                address = [address stringByAppendingString:placemark.subThoroughfare];
                address = [address stringByAppendingString:@" "];
            }
            
            //Street name (ex: Main Street, Timelesss Run)
            if (placemark.thoroughfare){
                address = [address stringByAppendingString:placemark.thoroughfare];
            }
            if (address.length){
                self.address.text = address;
            }else{
                self.address.text = @"Address not found";
            }
        }
    }];
}

#pragma mark - MainStore delegate

//Protocol method that gets called every time there is a location from other users
-(void)updateUserLocation:(Location *)location {
    //loop through and see if this user is already on the map
    __block BOOL isFound = NO;
    [annotations enumerateObjectsUsingBlock:^(MyCustomAnnotation *annotation, NSUInteger idx, BOOL *stop) {
        if ([annotation.location isEqual:location]){
            annotation.coordinate = location.locationCoords;
            annotation.location = location;
            isFound = YES;
            *stop = YES;
        }
    }];

    if (isFound) return;

    //This user does not exist on the map and we need to add it to the array and add it to the mapview
    MyCustomAnnotation *myCustomAnnotation = [[MyCustomAnnotation alloc] initWithLocation:location];
    
    myCustomAnnotation.coordinate = location.locationCoords;
    [annotations addObject:myCustomAnnotation];
    [self.mapView addAnnotation:myCustomAnnotation];


}

//The logged in user was tagged. The store method filters out all other tags
-(void)userTagged:(User *)taggedBy message:(NSString *)message {
    NSString *alertMessage = [NSString stringWithFormat:@"You have been tagged by %@!", taggedBy.userDisplayName];
    
    if (message.length) {
        alertMessage = [alertMessage stringByAppendingString:@"\n\n"];
        alertMessage = [alertMessage stringByAppendingString:@"Message: "];
        alertMessage = [alertMessage stringByAppendingString:message];
    }
    [[[UIAlertView alloc] initWithTitle:@"Tagged!" message:alertMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
}


#pragma mark - Location Manager
//Device location changed
-(void) locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //Make sure the user is logged in and we have already initlized the store
    if ([AppSession sharedSession].user && mainStore) {
        [mainStore sendNewLocation:newLocation];
    }
}

//Notifiy the user that the location service permisssions are turned off for this app
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
    NSLog(@"%@",error.userInfo);
    if([CLLocationManager locationServicesEnabled]){

        
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:@"Location Permission Denied"
                                                               message:@"To re-enable, please go to Settings app and turn on Location Service for this app."
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
            [alert show];
        }
    }
}

#pragma mark - Mapkit
//Mapkit protocol method to fetch what the annotation should look like
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if([annotation isKindOfClass:[MyCustomAnnotation class]]) {
        static NSString *identifier = @"pulsingAnnotationView";
        //Nice annotation libarary with a nice pulsing view (like my location icon)
        SVPulsingAnnotationView *pulsingAnnotationView = (SVPulsingAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        if(pulsingAnnotationView == nil) {
            pulsingAnnotationView = [[SVPulsingAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            pulsingAnnotationView.annotationColor = [UIColor colorWithRed:0.678431 green:0 blue:0 alpha:1];
        }
        
        pulsingAnnotationView.canShowCallout = NO;
        return pulsingAnnotationView;
    }
    
    return nil;
}
//Keep reference of the selected annotation
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)aView
{
    if (![aView.annotation isKindOfClass:[MyCustomAnnotation class]]) return;
    
    MyCustomAnnotation *myCustomAnn = (MyCustomAnnotation *)aView.annotation;
    selectedLocation = myCustomAnn.location;
    
    [self showTagView];
    [self setTagViewUser];
}


#pragma mark - Misc
//Let there be a white text status bar!
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

@end
