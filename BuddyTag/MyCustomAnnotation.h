//
//  MyCustomAnnotation.h
//  BuddyTag
//
//  Created by sj singh on 3/14/14.
//  Copyright (c) 2014 AppDar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Location.h"
#import <MapKit/MapKit.h>

@interface MyCustomAnnotation : NSObject <MKAnnotation>

@property (nonatomic,nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) Location *location;

- (id)initWithLocation:(Location *)theLocation;

@end
