//
//  ViewController.h
//  BuddyTag
//
//  Created by sj singh on 3/13/14.
//  Copyright (c) 2014 AppDar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MainStore.h"
#import "AppSession.h"
#import "LoginViewController.h"
#import <MapKit/MapKit.h>
#import "SVPulsingAnnotationView.h"
#import "MyCustomAnnotation.h"

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *tagView;

@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *address;
@property (strong, nonatomic) IBOutlet UITextView *tagMessage;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tagViewTrailingContraint;

@end
