//
//  AppSession.h
//  BuddyTag
//
//  Created by sj singh on 3/13/14.
//  Copyright (c) 2014 AppDar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface AppSession : NSObject

+ (AppSession *)sharedSession;

@property (nonatomic, strong) User *user;

@end
