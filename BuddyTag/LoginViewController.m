//
//  LoginViewController.m
//  BuddyTag
//
//  Created by sj singh on 3/13/14.
//  Copyright (c) 2014 AppDar. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@property (strong, nonatomic) IBOutlet UITextField *displayName;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;


@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self.displayName becomeFirstResponder];
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)login:(id)sender {
    if (self.displayName.text.length < 4){
        [[[UIAlertView alloc] initWithTitle:@"Name" message:@"Please enter a valid name that is longer than 3 characters." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return;
    }
    
    User *user = [User new];
    //Since we do not have a backend with a db, we will be generating a random number on the client... fingers crossed and hope for the best!
    user.userID = [NSNumber numberWithLong:(arc4random() % 99999999) ];
    user.userDisplayName = self.displayName.text;
    [AppSession sharedSession].user = user;
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}


@end
