//
//  AppSession.m
//  BuddyTag
//
//  Created by sj singh on 3/13/14.
//  Copyright (c) 2014 AppDar. All rights reserved.
//

#define USER_ARCHIVE_KEY @"loggedInUserArchivedKey"
#import "AppSession.h"

@interface AppSession () {
    User *_user;
}

@end

@implementation AppSession

static AppSession *theSharedStore = nil;

+ (AppSession *)sharedSession {
    if (!theSharedStore)
        theSharedStore = [[AppSession alloc] init];

    return theSharedStore;
}


-(User *)user {
    if (!_user){
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:USER_ARCHIVE_KEY];
        if (!data) return nil;
        _user = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    
    return _user;
}

-(void)setUser:(User *)user {
    _user = user;
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:_user];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:USER_ARCHIVE_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
