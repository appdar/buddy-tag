//
//  User.m
//  BuddyTag
//
//  Created by sj singh on 3/13/14.
//  Copyright (c) 2014 AppDar. All rights reserved.
//

#import "User.h"

@implementation User

+ (User *) userFromJSONDict: (NSDictionary *) jsonDict {
    User *user = [User new];
    
    user.userID = jsonDict[@"id"];
    user.userDisplayName = jsonDict[@"name"];
    
    return user;
}

-(BOOL)isEqual:(User *)object {
    if (![object isKindOfClass:[User class]]) return NO;
    
    if ([object.userID isEqualToNumber:self.userID]) return YES;
    else return NO;
}

#pragma mark NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_userDisplayName forKey:@"_userDisplayName"];
    [aCoder encodeObject:_userID forKey:@"_userID"];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        _userID = [aDecoder decodeObjectForKey:@"_userID"];
        _userDisplayName = [aDecoder decodeObjectForKey:@"_userDisplayName"];
    }
    return self;
}

@end
