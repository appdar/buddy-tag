//
//  MainStore.h
//  BuddyTag
//
//  Created by sj singh on 3/13/14.
//  Copyright (c) 2014 AppDar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "User.h"
#import "Location.h"
#import "Tag.h"
#import "AppSession.h"

@protocol MainStoreDelegate <NSObject>

@optional
- (void)updateUserLocation:(Location *)location;
- (void)userTagged:(User *)taggedBy message:(NSString *)message;

@end


@interface MainStore : NSObject

- (void)connectToPubnub;
- (void)sendNewLocation:(CLLocation *)location;
- (void)tagUser:(User *)user message: (NSString *)message;

@property (nonatomic, weak) id<MainStoreDelegate> delegate;

@end
